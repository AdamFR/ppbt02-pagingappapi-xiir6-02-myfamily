package main
import "encoding/json"
import "net/http"
import "fmt"

type myfamily struct {
	ID string
	Nama string
	Umur int
}

var data = []myfamily{
	myfamily{"1", "Ayah Adam", 54},
	myfamily{"2", "Ibu Adam", 40},
	myfamily{"3", "Adik Adam", 10},
	myfamily{"4", "Kakak Adam", 25},

	
}

func murid(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	if r.Method == "POST" {
		// var habibganteng = data[0] 
		var result, err = json.Marshal(data)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Write(result)
		return
	}
	http.Error(w, "", http.StatusBadRequest)
}

func user(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	if r.Method == "POST" {
		var id = r.FormValue("id")
		var result []byte
		var err error
		for _, each := range data {
			if each.ID == id {
				result, err = json.Marshal(each)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				w.Write(result)
				return
			}
		}
		http.Error(w, "User not found", http.StatusBadRequest)
		return
	}
	http.Error(w, "", http.StatusBadRequest)
}

func main() {
	http.HandleFunc("/murid", murid)
	// http.HandleFunc("/users/page=1", users)
	http.HandleFunc("/user", user)
	fmt.Println("starting web server at http://localhost:8080/")
	http.ListenAndServe(":8080", nil)
}